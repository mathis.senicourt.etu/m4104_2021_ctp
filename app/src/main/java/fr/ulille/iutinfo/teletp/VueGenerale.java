package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1

    private String DISTANCIEL;
    private String poste;
    private String salle;

    // TODO Q2.c
    private SuiviViewModel suiviViewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;
        // TODO Q2.c
        suiviViewModel = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spinnerSalle = view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(super.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSalle.setAdapter(adapter);
        Spinner spinnerPoste = view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterP = ArrayAdapter.createFromResource(super.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapterP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPoste.setAdapter(adapterP);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView login = (TextView) getActivity().findViewById(R.id.tvLogin);
            suiviViewModel.setUsername(login.getText()+"");

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });


        // TODO Q5.b
        spinnerSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Update d'un élément");
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                System.out.println("Update de élement");
                update();
            }
        });
        update();

        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        Spinner spinnerSalle = (Spinner) getView().findViewById(R.id.spSalle);
        Spinner spinnerPoste = (Spinner) getView().findViewById(R.id.spPoste);
        if (spinnerSalle.getSelectedItem().toString().equals(DISTANCIEL)) {
            spinnerPoste.setVisibility(View.INVISIBLE);
            spinnerPoste.setEnabled(false);
        } else {
            spinnerPoste.setVisibility(View.VISIBLE);
            spinnerPoste.setEnabled(true);
        }
        if (spinnerSalle.getSelectedItem() != null && spinnerPoste.getSelectedItem() != null) {
            SuiviViewModel.setLocalisation(getResources().getStringArray(R.array.list_salles)[spinnerSalle.getSelectedItemPosition()] + " : " + getResources().getStringArray(R.array.list_postes)[spinnerPoste.getSelectedItemPosition()]);
        } else {
            SuiviViewModel.setLocalisation(DISTANCIEL);
        }

    }


    // TODO Q9
}
